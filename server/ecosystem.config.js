'use strict';

// https://pm2.io/doc/en/runtime/guide/ecosystem-file/
module.exports = {
	'apps' : [ {
		'name': 'Zooniverse Export Tool: Server',
		'script': 'server.js',
		'env': {
			'DEBUG': 'hex:*'
		}
	} ]
};
