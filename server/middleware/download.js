'use strict';
// @flow
const fs = require('fs');
const sizeStr = require('filesize.js');

import type { Context } from 'express-hex/types/hex';

type DownloadMeta = {
	[string]: {
		file: string,
		sha1sum: string,
		version: string
	}
};

type AnnotatedMetaEntry = {
	file: string,
	sha1sum: string,
	version: string,
	size: string,
	label: string,
	active: boolean
};

// @TODO may need to support multiarch for other platforms, pending feedback
const labels = {
	'linux': {
		'platform': 'Linux',
		'arch': 'x86 64-bit'
	},
	'win': {
		'platform': 'Windows'
	},
	'darwin': {
		'platform': 'Mac OS X'
	}
};

module.exports = ({ app, conf }: Context) => {
	const metaPath = `${ __dirname }/../public/packages/meta.json`;
	// avoid require()ing to avoid caching complications
	const readMeta = () => {
		const meta: DownloadMeta = JSON.parse(fs.readFileSync(metaPath, { 'encoding': 'utf8' }));
		Object.keys(meta).forEach((platform) => {
			const entry: AnnotatedMetaEntry = (meta[platform]: any);
			entry.size = sizeStr(fs.statSync(`${ __dirname }/../public/packages/${ meta[platform].file }`).size);
			entry.label = labels[platform];
		});
		app.locals.meta = meta;
	};
	readMeta();
	// update on change
	fs.watch(`${ __dirname }/../public/packages/meta.json`, readMeta);
	const sourceUrl = conf.get('source-url', () =>
		fs.existsSync(`${ __dirname }/../../package.json`)
			? JSON.parse(
				fs.readFileSync(`${ __dirname }/../../package.json`, { 'encoding': 'utf8' })
			).repository
			: null
	);

	app.get([ '/', '/download' ], (req, res) => {
		// guess which OS the client is using. doesn't matter much if this is wrong or
		// they want to dl for another platform as the other options are still available
		let activeOs = 'win';
		if (req.headers['user-agent']) {
			if ((/\WLinux\W/).test(req.headers['user-agent'])) {
				activeOs = 'linux';
			}
			if ((/\WMacintosh\W/).test(req.headers['user-agent'])) {
				activeOs = 'darwin';
			}
		}

		res.render('download', {
			sourceUrl,
			'meta': Object.entries(app.locals.meta)
				.map(([ platform, info ]) => {
					(info: any).active = platform === activeOs;
					return info;
				})
				// active first, then alphabetical
				.sort((first, second) => {
					const fst: AnnotatedMetaEntry = (first: any);
					const snd: AnnotatedMetaEntry = (second: any);
					if (fst.active) {
						return -1;
					}
					if (snd.active) {
						return 1;
					}
					return fst.label > snd.label ? 1 : -1;
				})
		});
	});
};

