'use strict';
// @flow
const compareVersions = require('compare-versions');
const sqlite = require('sqlite');

import type { Context } from 'express-hex/types/hex';

module.exports = async ({ app, conf }: Context) => {
	const dbh = app.locals.dbh = await sqlite.open(`${ __dirname }/../data/log.sqlite`, { Promise });
	await dbh.migrate();

	const log = (type, machineId, payload, errorId) => dbh.run(
		'INSERT INTO log(type, machine_id, payload, error_id) VALUES (?, ?, ?, ?)',
		[ type, machineId, typeof payload === 'string' ? payload : JSON.stringify(payload), errorId ]
	);

	const HTTP_NO_CONTENT = 204;
	const HTTP_OK = 200;

	app.get('/check-update/:platform(\\w+)/:version(\\d+[.]\\d+[.]\\d+)', (req, res, next) => {
		if (!app.locals.meta[req.params.platform]) {
			return next();
		}

		const id = Object.keys(req.query)[0];
		const available = app.locals.meta[req.params.platform].version;
		log('check-update', id, {
			available,
			'platform': req.params.platform,
			'current': req.params.version
		});
		console.log('check-update', id, { available, 'platform': req.params.platform, 'current': req.params.version });

		res.type('json');

		const updateAvailable = compareVersions(req.params.version, available) === -1;
		if (updateAvailable) {
			return res.type('text').send(JSON.stringify({
				'version': app.locals.meta[req.params.platform].version,
				'url': conf.get('passport.strategies.google.callback').replace(/\/login\/google\/callback$/, '')
			}));
		}
		return res.status(HTTP_NO_CONTENT).send();
	});

	app.post('/report/:type(unresponsive|exception|crashed)', (req, res) => {
		const errId = req.body.errorId;
		if (errId) {
			delete req.body.errorId;
		}
		log(req.params.type, Object.keys(req.query)[0], req.body, errId);
		res.status(HTTP_OK).send();
	});
};
