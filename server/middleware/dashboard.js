'use strict';
// @flow

import type { Context } from 'express-hex/types/hex';

module.exports = ({ app, conf, log }: Context) => {
	const accounts = conf.get('dashboard.accounts', []);
	log.info('\t', { accounts });

	// don't bother enabling middleware if inaccessible
	if (accounts.length === 0) {
		return;
	}

	const HTTP_FORBIDDEN = 403;

	app.get('/dashboard',
		// login protect
		(req, res, next) => {
			if (!req.user) {
				return res.redirect(`/login?return=${ encodeURIComponent('/dashboard') }`);
			}
			if (!req.user.email || !accounts.includes(req.user.email)) {
				return res.status(HTTP_FORBIDDEN).type('text').send('Forbidden');
			}
			return next();
		},
		// show dashboard
		async (req, res) => {
			// @TODO pagination
			const { errorCount } = await app.locals.dbh.get('SELECT COUNT(DISTINCT error_id) AS errorCount FROM log');
			const errors = await app.locals.dbh.all(
				`SELECT min(created) AS first, max(created) AS last, count(error_id) AS count, error_id, payload
				FROM log WHERE error_id IS NOT NULL GROUP BY error_id ORDER BY last DESC`
			);
			res.render('dashboard', {
				errorCount,
				errors
			});
		}
	);
};

