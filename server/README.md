# zooniverse-export-tool: server

This server is meant to serve downloads of the packaged binaries of the export tool, to communicate with clients to check when updates are available, and to log exceptions that occur in the tool for quality assurance.

## Installation
 * Install nodejs from your package manager
 * `npm install -g yarn`
 * Run `yarn install` in the `server/` path
 * For production only, `yarn global add pm2`

## Configuration

You'll need a Google API key for authentication to the dashboard portion of the server.

Log into the API dashboard: https://console.developers.google.com

Create a project, or re-use an existing one. In either case make sure the Google+ API is activated for the project.

Visit the credentials page https://console.developers.google.com/apis/credentials

Either click an existing client ID or click the button to create a new one. You must choose "web application" for the type.

In the list of redirect URLs, add `https://${ YOUR DOMAIN }/login/google/callback`, filling in the domain.

Copy your client ID and secret for the next step.


### `server/secrets.js`

Create this file and fill in the appropriate content. This isn't verbatim, just a guide:

```javascript
module.exports = {
	// list any Google accounts you wish to be able to log in and look at the dashboard,
	// which shows a log of runtime errors and an anonymous usage overview.
	'dashboard.accounts': [ 'foo@gmail.com', ... ],
	// you can use `node node_modules/hex-session/bin/keygen` to generate a suitable secret
	'session.secret': 'longish random string',
	'passport.strategies.google': {
		// from the Google API console above
		'client': {
			'id': '557426451127-nnv91rhs44dbk37dh956vqrj3mbcnq6q.apps.googleusercontent.com',
			'secret': '...'
		},
		// fill in your domain. also, if you are serving the application from a base path you can add that before /login,
		// e.g., https://somedomain.org/export-tool/login/google/callback
		'callback': 'https://${ YOUR DOMAIN }/login/google/callback'
	}
};
```

### Content security policy

`index.html` and `file.html` both define a CSP with a <meta> tag. Amend these both to include the domain you're using as part of the `default-src`

## Running

For dev or testing, you can run `DEBUG=hex:* yarn start`. You can use `DEBUG=* to also see debugging messages from other components.

In production, you should set up an init script from `pm2`, a process manager that will watch the server: https://pm2.io/doc/en/runtime/guide/startup-hook/

You should use the version where you supply a user like `www-data` to own the process and its resources.

`chown` the `server/` path to the user you chose, then, as that user run `pm2 start ./ecosystem.config.js`.

This will output some logs to the user's `~/.pm2/logs` which you can consult in case something goes awry here.

Otherwise, run `pm2 save`. This will cause the init script to resurrect the services on reboot. It'll also restart it if it crashes.
