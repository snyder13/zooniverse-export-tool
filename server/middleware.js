'use strict';
// @flow

module.exports = {
	'download': {
		'description': 'Download options for the client. Connects with `scripts/publish` from the Electron package',
		'deps': [ 'hex-views-handlebars.engine' ]
	},
	'events': {
		'description': 'Record errors and update checks',
		'deps': [ 'hex.body-parser' ]
	},
	'dashboard': {
		'description': 'Authorized users can view events. Add accounts to conf \'dashboard.accounts\': [ \'foo@gmail.com\', ... ]',
		'deps': [ 'hex-authn-passport.google', 'events' ]
	}
};
