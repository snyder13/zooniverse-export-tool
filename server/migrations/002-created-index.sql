-- Up
CREATE INDEX created_idx ON log(created)

-- Down
DROP INDEX created_idx;
