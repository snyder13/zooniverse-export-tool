-- Up
CREATE INDEX type_idx ON log(type)

-- Down
DROP INDEX type_idx;
