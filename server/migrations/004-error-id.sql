-- Up
ALTER TABLE log ADD COLUMN error_id text;
CREATE INDEX error_id_idx ON log(error_id)

-- Down
DROP INDEX error_id_idx;
ALTER TABLE log DROP COLUMN error_id;
