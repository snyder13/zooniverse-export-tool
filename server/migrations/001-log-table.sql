-- Up
CREATE TABLE log(
	created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	machine_id char(64) NULL,
	type varchar(50) NOT NULL,
	payload TEXT NOT NULL
)

-- Down
DROP TABLE log;
