module.exports = exports = {
	'parser': 'babel-eslint',
	'parserOptions': {
		'sourceType': 'module',
		'ecmaFeatures': {
			'modules': true,
		}
	},
	'rules': {
		// should error ideally, but mixing 'require' and 'import type' isn't working properly
		'strict': 'off',
		'no-console': 'off',
		'no-process-exit': 'off'
	}
};
