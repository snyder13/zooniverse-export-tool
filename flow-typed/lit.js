declare type LitProp = {
	type: Object | (string) => any | {
		fromAttribute: (string) => any,
		toAttribute: (any) => string
	},
	attribute?: boolean | string, // true (default) reflect to lowercased attribute, false to not, string to specify alternate attribute name
	reflect?: boolean,
	hasChanged?: (newValue: any, oldValue: any) => boolean
}
declare type LitProps = {
	[string]: LitProp
}
