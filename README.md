# Zooniverse Export Tool

Helper to ingest data exports, show some summary data about them, and offer different export options to aid in different workflows.

![example animation](./zn-anim.gif)

Ingesting works, summary data is fairly fleshed-out, and export options are TODO.

## Installing
[Beta packages available here](https://zooniverse.rmer.info)

On OS X and Linux you may be prompted to install the application. You don't have to, you can run it as-is.

On OS X and Windows you must go through an extra step to run the application since it is unsigned.

For OS X, open the `dmg` to arrive at a window with the Electron icon on the left and your Applications folder on the right, with the prompt to drag-to-install. Right click/two-finger tap on the Electron icon and choose open rather than double-clicking it. This will allow you to bypass the warning.
For Windows, open the `exe`, then when the warning window appears click "more info" to unlock the option to proceed.

*(These extra steps can be avoiding by purchasing signing keys: https://electronjs.org/docs/tutorial/code-signing)*

## Setting up for development (Linux, OS X & Windows)

### Dependencies
 * node.js (v10 tested)
 * yarn (`npm install -g yarn` if it's not in your package manager)
 * python 3 (3.6.x required to build portable environments, any 3.x ok for local dev work)
 * cx_Freeze (if distributing changes to the Python portions of the app, see below)

Dependencies tested from Debian's `apt`, OS X MacPorts, and Windows Chocolatey. Other package managers ought to work as well.

#### Dev tools

You need a compiler to build the ZeroMQ module for your platform.

On Debian, install the `build-essential` package.

For OS X or other \*nix, install a recent `gcc` from your package manager.

For Windows, run `npm install --global windows-build-tools`

### Installing
 * `yarn install`- JS libs
 * `yarn build` - Webpacks UI components in `src/` to `static/`. Run `yarn watch` in development to do this whenever `src/**/*` changes

#### Python environments

If you don't need to change the Python parts of the application, you can use the pre-made environments:

 * `yarn checkout-envs`

If you do need to rebuild Python changes, you can run:

 * `yarn python-venv`

This makes an environment that will only work on your local computer, but which is relatively convenient to work with (you don't have to re-make the environment for Python changes to be reflected.)

In order to distribute Python changes portably, you'll need to use cx_Freeze to generate an environment that bundles all the pertinent libraries:

1. Install cx_Freeze: `python3.6 -m pip install cx_Freeze --upgrade` (at time of writing cx_Freeze is not compatible with Python 3.7+)
2. `python3.6 -m pip install -r requirements.txt` from the repo root
3. `PYTHON3=python3.6 yarn python-env`


### Running
 * `yarn start-dev`

Press F12 to open the dev tools console.

### Development

#### File structure

##### `src/components`
This contains the bulk of the application, `lit-element` web components.

Each consists of an `index.js` that controls their behavior, a `template.js` that contains the visual representation of the component's state, and `style.pcss` for style rules.

You can add any PostCSS plugins you like to support SASS or whatever you'd like, the base config only adds local imports.

You can run `yarn watch` to automatically rebuilt thes files.

The JavaScript files here use [Flow types](https://flow.org/). These are opt-in on a few different levels. Having `// @flow strict` at the top of the file determines whether it is type-checked at all, and casting things to `any` allows you to opt out of specific instances of type-checking. (`(foo: any).doSomething()`)

##### `scripts`
These are Python scripts that do data-munging tasks and communicate with the Electron process via ZeroMQ. Only Python 3 is packaged for each platform, so they should work in that context.

By convention, `foo.py` exports an Python interface that does some sort of work, and can do anything you like when run directly on the command line (`__name__ == '__main__'). `foo-api.py` pairs with it to provide a ZeroMQ wrapper implementing that interface.

Any JavaScript here is employed as part of the build process.

##### `flow-typed`
Contains some basic types that make checking work. These are always included by `flow` and do not need to be specifically `import`-ed by code using them.

#### "bootstrap"-type files
|      file      | purpose |
| -------------- | ------- |
| `main.js`      | Launches the application |
| `index.html`   | Application entry point |
| `file.html`    | Single-file view spawned by index |
| `src/index.js` | Imports any web components that are used directly in one of those `.html` files. This components can import their own sub-components without them needing to be listed here. Just the entry points |

#### rc files

|        file         | purpose |
| ------------------- | ------- |
| `package.json`      | JavaScript deps, definitions for `yarn` scripts, general metadata |
| `.babelrc`          | Babel config, responsible primarily for removing Flow types during linting and Webpack-ing. Add more plugins here if you need to use ES features that aren't directly supported in the version of V8 that gets bundled |
| `.eslintrc`         | JavaScript linting standards. You can use comments to disable rules per-line or per-file when necessary |
| `.flowconfig`       | Contains enough context to make Flow type-check successfully |
| `postcss.config.js` | Contains PostCSS plugin declarations. Start here if you want to add support for [more stuff](https://github.com/postcss/postcss/blob/master/docs/plugins.md) |
| `pylintrc`          | Python linting rules. Also possible to disable w/ comments |
| `requirements.txt`  | Python deps |
| `.stylelintrc`      | PostCSS linting rules. Also possible to disable w/ comments |
| `webpack.config.js` | Config necessary to build a bundle from `src/`. Includings things like Flow type-stripping and `import` handlers for stylesheets |

### Packaging

#### Dependencies
 * OS X (virtual) machine - OS X can build for all three platforms while Windows and Linux can only cross-compile for each other.
 * Docker - https://docs.docker.com/docker-for-mac/install/
 * Completed Python environment setup above for each supported platform

#### Building
On the OS X host:

 0. Shut down any `yarn watch` instances you might have running, they can interfere.
 1. `yarn dist-mac` - build the OS X version
 2. `scripts/launch-dist-docker.sh` - Make sure the path you've cloned the repository in is listed among the shared folders in Docker's preferences.
 3. In Docker, `yarn install && yarn build`
 4. In Docker, run `yarn dist-linux && yarn dist-win`

The result should be portable (installation-optional) packages in dist: `exe` for Windows, `AppImage` for Linux, and `dmg` for OS X

#### Publishing

 * `node scripts/publish` - will ask for the location to copy the packages. You can provide it as an argument to the script for scripting purposes. Recognizes `SCP_PORT` environment variable if you run sshd on a different port.
