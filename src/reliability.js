import { remote } from 'electron';
import os from 'os';
import { machineIdSync } from 'node-machine-id';
import conf from '../conf';
import pkg from '../package.json';

const fwdEvent = (type) => (err) => {
	const error = JSON.stringify(err, Object.getOwnPropertyNames(err));
	console.error(err); // eslint-disable-line no-console
	let errorId = error;
	// if the error is an actual Error type, try to make a deduplicating ID by trimming off some context
	if (err.message && err.stack) {
		errorId = `${ err.name }: ${ err.message } ${
			// get the first "at Blah.blah (file://...:line), strip off the location info
			err.stack.split(/[\r\n]+/)[1].replace(/^\s+|[(].+/g, '')
		}`;
	}
	fetch(`${ conf['reliability-server'] }/report/${ type }?${ machineIdSync() }`, {
		'method': 'post',
		'headers': { 'Content-Type': 'application/json' },
		'body': JSON.stringify({
			errorId,
			error,
			'versions': process.versions,
			'os': {
				'id': machineIdSync(),
				'platform': os.platform(),
				'release': os.release(),
				'mem': [ os.freemem(), os.totalmem() ],
				'arch': os.arch()
			}
		})
	});
};

export const reportError = fwdEvent('exception');

const HTTP_OK = 200;
export const checkUpdate = async () => {
	try {
		const res = await fetch(
			`${ conf['reliability-server'] }/check-update/${
				os.platform().replace(/\d+$/, '')
			}/${ pkg.version }?${ machineIdSync() }`
		);
		if (res.status !== HTTP_OK) {
			return Promise.resolve(null);
		}
		return res.json();
	}
	catch (ex) {
		console.error(ex); // eslint-disable-line no-console
		return Promise.resolve(null);
	}
};

const F12_KEY_CODE = 123;
export default () => {
	const win = remote.getCurrentWindow();
	win.webContents.on('crashed', fwdEvent('crashed'));
	win.on('unresponsive', fwdEvent('unresponsive'));
	process.on('uncaughtException', fwdEvent('exception'));

	// F12 for dev tools
	document.addEventListener('keydown', (evt) => {
		if (evt.which === F12_KEY_CODE) {
			win.toggleDevTools();
		}
	});

	if (true || conf['auto-open-dev-tools']) {
		win.openDevTools();
	}
};
