// @flow
import type ZnRange from './index';
import { html } from 'lit-element';

export default (inst: ZnRange) => html`
<div id="prnt">
	<div id="mean" style="left: ${ inst.meanTextOff }px">
		<span class="num">${ inst.mean.toLocaleString([], { 'maximumFractionDigits': 4 }) }</span>
		<span class="label">mean</span>
	</div>
	<div id="min">${ inst.min }</div>
	<div id="bar">
		<div id="mean-marker" class="marker" style="left: ${ inst.meanOff }px"></div>
		<div id="median-marker" class="marker" style="left: ${ inst.medianOff }px"></div>
	</div>
	<div id="max">${ inst.max }</div>
	<div id="median" style="left: ${ inst.medianTextOff }px">
		<span class="num">${ inst.median }</span>
		<span class="label">median</span>
	</div>
</div>`;
