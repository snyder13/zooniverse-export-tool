// @flow strict
import css from './style.pcss';
import tpl from './template';
import { LitElement, html } from 'lit-element';

export type RangeStats = {
	mean: number,
	median: number,
	min: number,
	max: number
};

export default class ZnRange extends LitElement {
	meanOff: ?number
	medianOff: ?number
	meanTextOff: ?number
	medianTextOff: ?number
	min: number
	max: number
	mean: number
	median: number

	/**
	 * lit-managed properties
	 */
	static get properties(): LitProps {
		return {
			// public
			'min': {
				'type': Number
			},
			'max': {
				'type': Number
			},
			'mean': {
				'type': Number
			},
			'median': {
				'type': Number
			},
			// computed
			'meanOff': {
				'type': Number
			},
			'medianOff': {
				'type': Number
			},
			'meanTextOff': {
				'type': Number
			},
			'medianTextOff': {
				'type': Number
			},
		};
	}

	/**
	 * Compute derived properties once we know the public props & have effective widths for the pertinent text elements
	 */
	firstUpdated(): void {
		const bar = this.shadowRoot.querySelector('#bar');
		const barWidth = bar.clientWidth;
		const barPad = parseInt(window.getComputedStyle(bar).getPropertyValue('left'), 10);
		const spread = this.max - this.min;

		this.meanOff = Math.round(barWidth * ((this.mean - this.min) / spread));
		this.medianOff = Math.round(barWidth * ((this.median - this.min) / spread));

		this.meanTextOff = this.meanOff + barPad - this.shadowRoot.querySelector('#mean .num').clientWidth / 2;
		this.medianTextOff = this.medianOff + barPad - this.shadowRoot.querySelector('#median .num').clientWidth / 2;
	}

	/**
	 * @returns {html}
	 */
	render() {
		return html`<style>${ css }</style> ${ tpl(this) }`;
	}
}
window.customElements.define('zn-range', ZnRange);
