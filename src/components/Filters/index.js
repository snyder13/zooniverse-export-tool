// @flow strict
import css from './style.pcss';
import tpl from './template';
import { LitElement, html } from 'lit-element';
import flatpickr from 'flatpickr';

export default class ZnFilters extends LitElement {

	/**
	 * lit-managed properties
	 */
	static get properties(): LitProps {
		return {
			'calendarOpen': {
				'type': Boolean
			},
			'dateStart': {
				'type': String,
				'attribute': 'date-start'
			},
			'dateEnd': {
				'type': String,
				'attribute': 'date-end'
			}
		};
	}

	constructor() {
		super();
		this.calendarOpen = false;
	}

	firstUpdated() {
		this.dateRange = flatpickr(this.shadowRoot.querySelector('#date-range'), {
			'mode': 'range',
			'inline': true,
			'onClose': () => {
				this.calendarOpen = false;
				this.shadowRoot.querySelector('.flatpickr-calendar').classList.remove('open');
			}
		});
	}

	updated(changed: Set<string>) {
		if ((changed.has('dateStart') || changed.has('dateEnd')) && this.dateStart && this.dateEnd) {
			this.dateRange.setDate([ this.dateStart, this.dateEnd ]);
		}
		super.updated(changed);
	}

	pickDate() {
		this.calendarOpen = !this.calendarOpen;
		this.shadowRoot.querySelector('.flatpickr-calendar').classList.toggle('open');
	}

	apply() {
		this.dispatchEvent(new CustomEvent('change', {
			'detail': {
				'text': this.shadowRoot.querySelector('#filter').value,
				'dates': this.dateRange.selectedDates
			}
		}));
	}

	render() {
		return html`<style>${ css }</style> ${ tpl(this) }`;
	}
}
window.customElements.define('zn-filters', ZnFilters);
