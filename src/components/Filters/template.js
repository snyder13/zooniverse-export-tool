// @flow
import type ZnRange from './index';
import { html } from 'lit-element';

export default (inst: ZnFilters) => html`
	<div id="filters">
		<input id="date-range" @click="${ inst.pickDate.bind(inst) }" class="${ inst.calendarOpen ? 'open' : 'closed' }" />
		<input id="filter" placeholder="Filter for text..." />
		<button id="apply-filters" @click="${ inst.apply.bind(inst) }">Search</button>
	</div>
`;
