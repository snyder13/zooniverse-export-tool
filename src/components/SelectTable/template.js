// @flow strict
/* eslint-disable indent */
import type ZnSelectTable from './index';
import { html } from 'lit-element';

export default (inst: ZnSelectTable) => html`<table class="tbl">
	<thead>
		<tr>
			<th>
				Include (all:&nbsp;<input id="all" type="checkbox" ?checked="${ inst.all }" @click="${ inst.toggleAll.bind(inst) }" />)
			</th>
			${ inst.header.map((lbl) => html`<th>${ lbl }</th>`) }
		</tr>
	</thead>
	${
		inst.items.map((row, idx) => html`<tr>
			<td>
				<input
					type="checkbox"
					?checked="${ inst.isSelected[idx] }"
					data-index="${ idx }" @click="${ inst.toggle.bind(inst) }" />
			</td>
			${
				row.map((val) =>
					html`<td>${
						val.is && val.of
							? html`
								<span>${ val.is }</span>
								<div class="bar" style="width: ${ Math.round(100 * val.is / val.of) }%"></div>
							`
							: val
					}</td>`)
			}
		</tr>`)
	}
</table>`;
