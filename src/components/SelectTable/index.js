// @flow strict
import css from './style.pcss';
import tpl from './template';
import { LitElement, html } from 'lit-element';

export default class ZnSelectTable extends LitElement {
	static get
	properties() {
		return {
			'items': {
				'type': Array
			},
			'header': {
				'type': Array
			},
			'isSelected': {
				'type': Array
			},
			'name': {
				'type': String
			}
		};
	}

	updated(changed: Set<string>) {
		if (changed.has('items')) {
			this.isSelected = (new Array(this.items.length)).fill().map(() => true);
			this.dispatchEvent(new CustomEvent('change'));
		}
	}

	get
	selected() {
		return this.items.filter((_item, idx) => this.isSelected[idx]);
	}

	get
	allSelected() {
		return this.isSelected.every((selected) => selected);
	}

	toggleAll({ target }: { target: HTMLInputElement }) {
		this.isSelected = this.isSelected.map(() => target.checked);
		this.dispatchEvent(new CustomEvent('change'));
	}

	toggle({ target }: { target: HTMLInputElement }) {
		this.isSelected[target.dataset.index] = target.checked;
		this.dispatchEvent(new CustomEvent('change'));
	}

	render() {
		return html`<style>${ css }</style> ${ tpl(this) }`;
	}
}
window.customElements.define('zn-select-table', ZnSelectTable);
