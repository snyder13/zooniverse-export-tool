// @flow
import type ZnRange from './index';
import { html } from 'lit-element';

export default (inst: ZnToggleItem) => html`<div id="prnt" class="${ inst.selected ? 'selected' : '' }" @click="${ inst.signalChange.bind(inst) }">
	<slot></slot>
</div>`;
