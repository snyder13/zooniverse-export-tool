// @flow strict
import css from './style.pcss';
import tpl from './template';
import { LitElement, html } from 'lit-element';
import flatpickr from 'flatpickr';

export default class ZnToggleItem extends LitElement {

	/**
	 * lit-managed properties
	 */
	static get properties(): LitProps {
		return {
			'selected': {
				'type': Boolean
			},
			'name': {
				'type': String
			}
		};
	}

	signalChange() {
		if (!this.selected) {
			this.dispatchEvent(new CustomEvent('change', {
				'detail': this,
				'bubbles': true
			}));
		}
	}

	render() {
		return html`<style>${ css }</style> ${ tpl(this) }`;
	}
}
window.customElements.define('zn-toggle-item', ZnToggleItem);
