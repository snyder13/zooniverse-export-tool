// @flow
import type ZnRange from './index';
import { html } from 'lit-element';

export default (inst: ZnToggle) => html`<div id="prnt" @change="${ inst.changed.bind(inst) }">
	<slot id="items"></slot>
</div>`;
