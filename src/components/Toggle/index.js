// @flow strict
import css from './style.pcss';
import tpl from './template';
import { LitElement, html } from 'lit-element';
import flatpickr from 'flatpickr';

import './Item';

export default class ZnToggle extends LitElement {

	/**
	 * lit-managed properties
	 */
	static get properties(): LitProps {
		return {
			'value': {
				'type': String
			}
		};
	}

	firstUpdated() {
		// seed value from whichever item is marked `selected` (may not be any, but just in case)
		this.shadowRoot.querySelector('#items').addEventListener('slotchange', ({ target }) =>
			target.assignedNodes().some((node) => {
				if (node.selected) {
					this.setValue(node.name);
					return true;
				}
			})
		);
	}

	changed(evt) {
		evt.stopPropagation();
		// move selected attribute to only one item
		this.shadowRoot.querySelector('#items').assignedNodes().forEach((node) =>
			node.selected = node === evt.detail
		);
		this.setValue(evt.detail.name);
	}

	setValue(val) {
		this.value = val;
		this.dispatchEvent(new CustomEvent('change', {
			'detail': val,
			'bubbles': true,
			'composed': true
		}));
	}

	render() {
		return html`<style>${ css }</style> ${ tpl(this) }`;
	}
}
window.customElements.define('zn-toggle', ZnToggle);
