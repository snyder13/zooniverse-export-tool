// @flow strict
/* eslint-disable indent */
// import type ZnExport from './index';
import { html } from 'lit-element';
import './Json';

export default (inst: ZnExport) => html`
<p>Exported data reflects any applied date and text filters, and includes the columns and users selected on those tabs.</p>
<ul>
	<li>
		<h2>CSV</h2>
		${ inst.hasDeepColumnsSelected ? html`<p class="warning">Some document type columns are selected to export, those columns will be JSON-encoded.</p>` : '' }
		<button @click="${ () => inst.doExport('csv') }">Export CSV</button>
	</li>
	<li>
		<h2>JSON</h2>
		<zn-export-json></zn-export-json>
		<button @click="${ () => inst.doExport('json') }">Export JSON</button>
	</li>
</ul>`;
