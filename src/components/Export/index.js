// @flow strict
import css from './style.pcss';
import tpl from './template';
import { LitElement, html } from 'lit-element';

export default class ZnExport extends LitElement {
	static get
	properties() {
		return {
			'users': {
				'type': Array
			},
			'columns': {
				'type': Array
			},
			'sourceFile': {
				'type': String,
				'attribute': 'source-file'
			},
			'spawnPython': {
				'type': Object,
				'attribute': 'spawn-python'
			}
		};
	}

	doExport(type) {
		const opts = type === 'csv' ? {} : this.shadowRoot.querySelector('zn-export-json').value;
		this.spawnPython('export', { type, opts }, (doc, done) => {
			console.log('export response', doc);
			done();
		});
	}

	get
	hasDeepColumnsSelected() {
		return this.columns.selected.some(([ _, type ]) => type === 'document');
	}

	render() {
		return html`<style>${ css }</style> ${ tpl(this) }`;
	}
}
window.customElements.define('zn-export', ZnExport);
