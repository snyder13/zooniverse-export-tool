// @flow strict
import css from './style.pcss';
import tpl from './template';
import { LitElement, html } from 'lit-element';

const JSON_INDENT = 3;
const EXAMPLE_DATA = {
	'columns': [ 'A', 'B', 'C' ]
};
EXAMPLE_DATA.rows = (new Array(EXAMPLE_DATA.columns.length)).fill().map((_n, idx) =>
	EXAMPLE_DATA.columns.map((col) => `${ col }${ idx + 1 }`)
);

export default class ZnExportJson extends LitElement {
	static get
	properties() {
		return {
			'useHeader': {
				'type': Boolean
			},
			'jsonl': {
				'type': Boolean
			}
		};
	}

	constructor() {
		super();
		this.useHeader = true;
		this.jsonl = false;
	}

	get
	value() {
		return { 'header': this.useHeader, 'lines': this.jsonl };
	}

	get
	example() {
		const str = (obj) => JSON.stringify(obj, null, JSON_INDENT);
		if (this.useHeader) {
			if (this.jsonl) {
				return `${ JSON.stringify(EXAMPLE_DATA.columns) }\n${ EXAMPLE_DATA.rows.map(JSON.stringify).join('\n') }`;
			}
			return `{
	"columns": ${ JSON.stringify(EXAMPLE_DATA.columns) },
	"rows": [
		${ EXAMPLE_DATA.rows.map(JSON.stringify).join(',\n\t\t') }
	]
}`;
			return this.jsonl
				? str(EXAMPLE_DATA) + '(header jsonl)'
				: str(EXAMPLE_DATA) + '(header json)';
		}
		const rows = EXAMPLE_DATA.rows.map((row) =>
			row.reduce((acc, v, idx) => ({
				[ EXAMPLE_DATA.columns[idx] ]: v,
				...acc
			}), {})
		);
		return this.jsonl
			? rows.map(JSON.stringify).join('\n')
			: '[\n\t' + rows.map(JSON.stringify).join(',\n\t') + '\n]';
	}

	updateParams(evt) {
		this.useHeader = this.shadowRoot.querySelector('#header').value === 'on';
		this.jsonl = this.shadowRoot.querySelector('#jsonl').value === 'on';
	}

	render() {
		return html`<style>${ css }</style> ${ tpl(this) }`;
	}
}
window.customElements.define('zn-export-json', ZnExportJson);
