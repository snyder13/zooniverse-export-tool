// @flow strict
/* eslint-disable indent */
import ZnExportJson from './index';
import { html } from 'lit-element';

export default (inst: ZnExportJson) => html`
<zn-toggle id="header" @change="${ inst.updateParams.bind(inst) }">
	<zn-toggle-item selected name="on">List columns in header</zn-toggle-item>
	<zn-toggle-item name="off">List columns in each row</zn-toggle-item>
</zn-toggle>
<zn-toggle id="jsonl" @change="${ inst.updateParams.bind(inst) }">
	<zn-toggle-item selected name="off">One containing object</zn-toggle-item>
	<zn-toggle-item name="on">One line per row (JSONL)</zn-toggle-item>
</zn-toggle>
<h3>Example:</h3>
<pre id="example">${ inst.example }</pre>`;
