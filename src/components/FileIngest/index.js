// @flow strict
import css from './style.pcss';
import tpl from './template';
import { LitElement, html } from 'lit-element';
import { remote, shell } from 'electron';
import reliabilityHooks, { checkUpdate } from '../../reliability';

const FILE_WIN = {
	'width': 750,
	'height': 900,
	'minWidth': 750,
	'minHeight': 500
};

const fileWindows = [];

export default class ZnFileIngest extends LitElement {
	static get properties() {
		return {
			'updatePrompt': {
				'type': Object
			}
		};
	}

	constructor() {
		super();
		reliabilityHooks();
		checkUpdate().then((update) => {
			this.updatePrompt = update;
			this.requestUpdate();
		});
	}

	/**
	 * open a window to display the per-file interface, and send it the path to load
	 *
	 * @param {string} path location of the file
	 */
	openFile(path: string): void {
		const fw = new remote.BrowserWindow(FILE_WIN);

		fw.setMenu(null);
		fw.loadFile('file.html');

		// manage long-lived reference
		fileWindows.push(fw);
		const idx = fileWindows.length - 1;
		fw.on('closed', () => fileWindows[idx] = null);

		fw.webContents.on('did-finish-load', () =>
			fw.webContents.send('load', path)
		);
	}

	/**
	 * show native file dialog and read any selection(s)
	 */
	browse(): void {
		const files = remote.dialog.showOpenDialog({
			'title': 'Choose Zooniverse CSV+JSON export file(s)',
			'multiSelections': true,
			'filters': [ { 'name': 'Zooniverse Data Files', 'extensions': [ 'csv' ] } ]
		});
		if (files) {
			files.forEach(this.openFile);
		}
	}

	/**
	 * various drag-related events should have their default prevented to allow dropping files
	 *
	 * @param {DragEvent} evt event to be stopped
	 */
	cancelEvent(evt: DragEvent): void {
		evt.preventDefault();
	}

	/**
	 * read dropped files
	 *
	 * @param {DragEvent} evt drop event
	 */
	onDrop(evt: DragEvent): void {
		if (!evt.dataTransfer || !evt.dataTransfer.files) {
			return;
		}
		for (let idx = 0; idx < evt.dataTransfer.files.length; ++idx) {
			this.openFile((evt.dataTransfer.files[idx]: Object).path);
		}
	}

	getUpdate(evt: MouseEvent): void {
		evt.preventDefault();
		shell.openExternal(this.updatePrompt.url);
	}

	/**
	 * @returns {html}
	 */
	render() {
		return html`<style>${ css }</style> ${ tpl(this) }`;
	}
}
window.customElements.define('zn-file-ingest', ZnFileIngest);
