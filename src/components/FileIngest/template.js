// @flow
import type ZnFileIngest from './index';
import { html } from 'lit-element';

export default (inst: ZnFileIngest) => {
	// helper to return instance methods bound to the owner object so when called
	// `this` does not refer to the event context
	function bind(evt) {
		return inst[evt].bind(inst);
	}

	return html`
<div id="drop"
	@click="${ bind('browse') }"
	@dragover="${ bind('cancelEvent') }"
	@dragenter="${ bind('cancelEvent') }"
	@dragleave="${ bind('cancelEvent') }"
	@drop="${ bind('onDrop') }"
	>
	<p><span id="icon"></span> Drop data file(s) here, or click to browse</p>
</div>
${ inst.updatePrompt ? html`<div id="update-prompt" class="${ inst.updatePrompt ? 'active' : 'hidden' }">
	<a href="#" @click="${ bind('getUpdate') }">Updated version ${ inst.updatePrompt.version } is available</a>
</div>` : '' }
`;
};
