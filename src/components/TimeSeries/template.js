// @flow
import { html } from 'lit-element';
import type ZnTimeSeries from './index';

export default (inst: ZnTimeSeries) => html`
<div id="prnt">
	<div id="chart">
	</div>
	<div id="tool-tip" style="top: ${ inst.tipOffset.top }px; left: ${ inst.tipOffset.left }px">
		<h2>${ inst.tipDate }</h2>
		<table>
			<tbody>
				${ inst.tips.map(({ label, value }) => html`<tr><td>${ label }</td><td>${ value }</td></tr>`) }
			</tbody>
		</table>
	</div>
</div>`;
