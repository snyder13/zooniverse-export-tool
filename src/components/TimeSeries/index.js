// @flow strict
/* eslint-disable indent */ // d3 idiom often uses non-conforming indents
import css from './style.pcss';
import tpl from './template';
import { LitElement, html } from 'lit-element';
import * as d3 from 'd3';

export type TimeSeriesLine = {
	label: string,
	data: Array<[Date, number]>,
	interpolate?: string
};

const DEFAULT_HEIGHT_PX = 300;
const DEFAULT_WIDTH_PX = 600;
const TRANSITION_TIME_MS = 500;
const DEFAULT_MARGIN = {
	'TOP': 5,
	'RIGHT': 0,
	'BOTTOM': 20,
	'LEFT': 50
};
const TOOLTIP_CORRECT_Y_PX = 6;

export default class ZnTimeSeries extends LitElement {
	height: number
	width: number

	static get properties() {
		return {
			'lines': {
				'type': Array
			},
			'height': {
				'type': Number
			},
			'width': {
				'type': Number
			},
			'tips': {
				'type': Array
			},
			'tipDate': {
				'type': String
			},
			'tipOffset': {
				'type': Object
			},
			'domainYMin': {
				'type': Number
			},
			'domainYMax': {
				'type': Number
			}
		};
	}

	constructor() {
		super();
		this.height = DEFAULT_HEIGHT_PX;
		this.width = DEFAULT_WIDTH_PX;
		this.svgLines = [];
		this.tips = [];
		this.tipOffset = {
			'top': 0,
			'left': 0
		};
	}

	updated(changed: Map<string, any>) {
		super.updated(changed);
		if (!changed.has('lines')) {
			return;
		}
		this.setup();
		this.redraw();
	}

	redraw() {
		const domain = {
			'x': {
				'min': null,
				'max': null
			},
			'y': {
				'min': this.domainYMin ? this.domainYMin : null,
				'max': this.domainYMax ? this.domainYMax : null
			}
		};
		// moderately janky domain finding avoids copy data from all lines together into a big lump for d3.extent to handle directly
		this.lines.forEach(({ data }) => {
			data.forEach(([ date, n ]) => {
				domain.x.min = domain.x.min === null ? date : date < domain.x.min ? date : domain.x.min;
				domain.x.max = domain.x.max === null ? date : date > domain.x.max ? date : domain.x.max;

				domain.y.min = domain.y.min === null ? n : n < domain.y.min ? n : domain.y.min;
				domain.y.max = domain.y.max === null ? n : n > domain.y.max ? n : domain.y.max;
			});
		});
		this.scale.x.domain([ domain.x.min, domain.x.max ]);
		this.scale.y.domain([ domain.y.min, domain.y.max ]);

		const trans = d3.transition().duration(TRANSITION_TIME_MS);

		this.axisGroups.x
			.transition(trans)
			.call(this.axes.x);
		this.axisGroups.y
			.transition(trans)
			.call(this.axes.y);

		this.lines.forEach((def, idx) => {
			if (!this.svgLines[idx]) {
				const line = d3.line()
					.x((node) => this.scale.x(node[0]))
					.y((node) => this.scale.y(node[1]));

				if (def.interpolate) {
					line.curve(d3[`curve${ def.interpolate[0].toUpperCase() }${ def.interpolate.slice(1) }`]);
				}

				const path = this.body.append('path')
					.attr('class', 'line')
					.attr('d', line(def.data));

				Object.entries(def.svg ? def.svg : {}).forEach(([ key, val ]) =>
					path.attr(key, val)
				);
				this.svgLines[idx] = {
					line,
					path
				};
			}
			else {
				this.svgLines[idx].path
					.transition(trans)
					.attr('d', this.svgLines[idx].line(def.data));
			}
		});
	}

	setup() {
		if (this.body) {
			return;
		}
		const margin = {};
		Object.keys(DEFAULT_MARGIN).forEach((key) => margin[key.toLowerCase()] = DEFAULT_MARGIN[key]);

		const width = this.bodyWidth = this.width - margin.left - margin.right;
		const height = this.bodyHeight = this.height - margin.top - margin.bottom;

		this.scale = {
			'x': d3.scaleTime().range([ 0, width ]),
			'y': d3.scaleLinear().range([ height, 0 ])
		};

		this.axes = {
			'x': d3.axisBottom().scale(this.scale.x).tickFormat(d3.timeFormat('%d/%m')),
			'y': d3.axisLeft().scale(this.scale.y)
		};

		const svg = d3.select(this.shadowRoot.querySelector('#chart'))
			.append('svg')
				.attr('width', width + margin.left + margin.right)
				.attr('height', height + margin.top + margin.bottom);

		this.axisGroups = {
			'x': svg.append('g')
				.attr('class', 'x axis')
				.attr('transform', `translate(${ margin.left }, ${ height })`)
				.call(this.axes.x),
			'y': svg.append('g')
				.attr('class', 'y axis')
				.attr('transform', `translate(${ margin.left }, ${ margin.top })`)
				.call(this.axes.y)
		};

		this.body = svg.append('g')
			.attr('id', 'body')
			.attr('transform', `translate(${ margin.left }, ${ margin.top })`);

		const updateToolTip = this.updateToolTip.bind(this);
		const tipBox = this.shadowRoot.querySelector('#tool-tip');
		this.bisectDate = d3.bisector(([ date ]) => date).left;
		this.body.append('rect')
			.attr('id', 'mouse-bounds')
			.attr('width', width)
			.attr('height', height)
			.on('mousemove', () =>
				updateToolTip(d3.mouse(this))
			);

		this.focus = this.body.append('circle')
			.attr('id', 'focus');

		const prnt = this.shadowRoot.querySelector('#prnt');
		prnt.addEventListener('mouseover', () => {
			this.focus.style('opacity', 1);
			tipBox.classList.add('active');
		});
		prnt.addEventListener('mouseout', () => {
			this.focus.style('opacity', 0);
			tipBox.classList.remove('active');
		});

	}

	updateToolTip([ mouseX ]) {
		if (!this.lines) {
			return;
		}
		const date = this.scale.x.invert(mouseX);
		const idx = this.bisectDate(this.lines[0].data, date, 1);
		const d0 = this.lines[0].data[idx - 1];
		if (!d0) {
			return;
		}
		const d1 = this.lines[0].data[idx];
		const effIdx = date - d0[0] > d1[0] - date ? idx : idx - 1;
		const dat = this.lines[0].data[effIdx];
		this.focus
			.attr('transform', `translate(${ this.scale.x(dat[0]) }, ${ this.scale.y(dat[1]) })`);

		this.tipOffset = {
			'top': this.scale.y(dat[1]) - this.shadowRoot.querySelector('#tool-tip').offsetHeight - TOOLTIP_CORRECT_Y_PX,
			'left': this.scale.x(dat[0])
		};

		this.tipDate = `
			${ [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ][dat[0].getDay()] },
			${ [ dat[0].getFullYear(), dat[0].getMonth() + 1, dat[0].getDate() ].join('/') }
		`;
		this.tips = this.lines.map(({ label, data }) => ({
			label,
			'value': data[effIdx][1].toLocaleString()
		}));
	}

	render() {
		return html`<style>${ css }</style> ${ tpl(this) }`;
	}

}
window.customElements.define('zn-time-series', ZnTimeSeries);
