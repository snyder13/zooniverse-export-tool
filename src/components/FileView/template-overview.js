// @flow strict
/* eslint-disable indent */
/* eslint-disable max-len */
import type ZnFileView from './index';
import { html } from 'lit-element';

import '../TimeSeries';

const NOTABLE_PCT = 0.1;

export default (inst: ZnFileView) => html`
<table id="summary">
	<tbody>
		<tr>
			<td>Classifications</td>
			<td>
				<table>
					<thead><tr><th></th><th>First ID</th><th>Last ID</th></tr></thead>
					<tbody>
						<tr>
							<td>
								<table>
									<tbody>
										<tr>
											<td colspan="2">${ inst.getNumber('classifications.count') } total</td>
										</tr>
										<tr>
											<td>${ inst.getNumber('detail.ClassificationObserver.logged_in.true') } logged in</td>
											<td>${ inst.getNumber('detail.ClassificationObserver.logged_in.false') } not</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td>${ inst.getNumber('classifications.min') }</td>
							<td>${ inst.getNumber('classifications.max') }</td>
						</tr>
						<tr>
							<td colspan="3">
								<p class="chart-label">Daily classifications, total and logged-in</p>
								<zn-time-series width="520" height="200" .lines="${ inst.classDailyLines }"></zn-time-series>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>Users</td>
			<td>
				<table>
					<tbody>
						<tr>
							<td>
								<table>
									<tbody>
										<tr>
											<td colspan="2">${ inst.getTotalUsers() } total</td>
											<td>${ inst.getNumber('users.ips') } unique IPs</td>
										</tr>
										<tr>
											<td>${ inst.getNumber('users.logged_in.true') } logged in</td>
											<td>${ inst.getNumber('users.logged_in.false') } not</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<p class="chart-label">Classifications per user</p>
								${ inst.getRange('detail.ClassificationObserver.per_user') }
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<p class="chart-label">Gini coefficient, daily and cumulative</p>
								<zn-time-series width="520" height="200" .lines="${ inst.classDailyGini }" domainYMin="0" domainYMax="1"></zn-time-series>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>Subjects</td>
			<td>
				<table>
					<tbody>
						<tr>
							<td>${ inst.getNumber('subjects.count') } total</td>
						</tr>
						<tr>
							<td colspan="2">
								<p class="chart-label">Classifications per subject</p>
								${ inst.getRange('detail.ClassificationObserver.per_subject') }
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>Workflows</td>
			<td>
				<table>
					<thead class="workflow full"><tr><th></th><th>Version</th><th>Occurrences</th></tr></thead>
					<tbody>
						${ Object.entries(inst.get('detail.WorkflowObserver', {})).map(([ id, versions: { [string]: number } ]) => html`
							<tr><td>${ id }</td><td colspan="2"></td></tr>
							${ inst.partitionByNotability((versions: any), NOTABLE_PCT,
								({ version, count }) => html`<tr><td></td><td>${ version }</td><td>${ count.toLocaleString() }</td></tr>`,
								(negligible) => html`<tr class="workflow-version"><td></td><td>${ negligible.join(', ') }</td><td>&lt; ${ NOTABLE_PCT }% combined</td></tr>`
							) }
						`) }
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>`;
