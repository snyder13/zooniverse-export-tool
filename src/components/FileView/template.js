// @flow strict
/* eslint-disable indent */
/* eslint-disable max-len */
import type ZnFileView from './index';
import { html } from 'lit-element';
import overview from './template-overview';

import '../TimeSeries';
import '../Export';
import '../SelectTable';
import '../Filters';
import '../Toggle';

export default (inst: ZnFileView) =>  html`
<div id="prnt" class="${ inst.loadID ? 'loading' : '' }">
	<div id="loader"></div>
	<div id="content">
		<ul id="tabs" @click="${ inst.changeTab.bind(inst) }">
			<li class="overview active">
				<span>Overview</span>
			</li>
			<li class="columns">
				<span>Columns</span>
			</li>
			<li class="users">
				<span>Users</span>
			</li>
			<li class="export">
				<span>Export</span>
			</li>
		</ul>
		<ul id="bodies">
			<li id="overview" class="active">
				${ overview(inst) }
			</li>
			<li id="columns">
				<zn-select-table name="columns" @change="${ inst.updateExportParams.bind(inst) }" .items="${
					Object.entries(inst.get('detail.TypesObserver', {})).map(([ name, type ]) => ([
						name, inst.typeMap[(type: any)], inst.data
							? { 'is': inst.data.unique_counts[name], 'of': inst.data.classifications.count }
							: ''
					]))
				}" .header="${ [ 'Column', 'Type', 'Unique Values' ] }"></zn-select-table>
			</li>
			<li id="users">
				<zn-select-table name="users" @change="${ inst.updateExportParams.bind(inst) }" .items="${
					inst.get('users.ranked', []).map(([ name, count ]) => ([
						name,
						inst.data ? { 'is': count, 'of': inst.data.users.ranked[0][1] } : ''
					]))
				}" .header="${ [ 'User', 'Contributions' ] }"></zn-select-table>
			</li>
			<li id="export">
				<zn-export source-file="${ inst.name }" .spawnPython="${ inst.spawnPython.bind(inst) }"></zn-export>
			</li>
		</ul>
	</div>
	<zn-filters date-start="${ inst.dateRange.start }" date-end="${ inst.dateRange.end }" @change="${ inst.applyFilters.bind(inst) }"></zn-filters>
</div>`;
