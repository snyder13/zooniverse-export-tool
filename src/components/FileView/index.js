// @flow strict
import css from './style.pcss';
import tpl from './template';
import { LitElement, html } from 'lit-element';
import { ipcRenderer, remote } from 'electron';
import path from 'path';
import zmq from 'zeromq';
import { spawn } from 'child_process';
import crypto from 'crypto';
import reliabilityHooks, { reportError } from '../../reliability';

import ZnRange from '../Range';

import type { TimeSeriesLine } from '../TimeSeries';
import type { RangeStats } from '../Range';

const DEFAULT_FRACTION_DIGITS = 4;
const PORT_START = 85329;
const PORT_SPAN = 100;
const MSG_ID_BYTES = 16;

export default class ZnFileView extends LitElement {
	path: string
	typeMap: {
		[string]: string
	}
	estLines: number
	data: ?{
		classifications: {
			count: number,
			min: string,
			max: string
		},
		dates: {
			start: string,
			end: string
		},
		detail: {
			ClassificationObserver: {
				logged_in: { false: number, true: number },
				per_subject: RangeStats,
				per_user: RangeStats
			},
			TypesObserver: {
				[string]: string // column name: str|dict|float
			},
			WorkflowObserver: {
				[number]: { // IDinst.cancelEvent.bind(inst)
					[string]: number // version: count
				}
			},
			TimeSeriesObserver: {
				[string]: { // YYYY-MM-DD
					mean_classification_duration: number,
					logged_in: { false: number, true: number },
					gini: {
						daily: number,
						cumulative: number
					}
				}
			}
		},
		final: boolean,
		subjects: {
			count: number
		},
		unique_counts: {
			[string]: number
		},
		users: {
			logged_in: { false: number, true: number },
			ips: number,
			ranked: [
				[string, number]
			],
			gini_coefficient_classification: number
		}
	}

	static get properties(): LitProps {
		return {
			'data': {
				'type': Object
			},
			'name': {
				'type': String
			},
			'dateRange': {
				'type': Object
			}
		};
	}

	/**
	 * Set some defaults, listen for message about file path
	 */
	constructor() {
		super();
		reliabilityHooks();

		// placeholder values for initial draw
		// python type > more "human-readble"
		this.typeMap = {
			'str': 'text',
			'float': 'number',
			'dict': 'document'
		};
		this.estLines = 0;
		this.dateRange = {
			'start': '',
			'end': ''
		};

		ipcRenderer.on('load', (_evt, filePath) => {
			this.path = filePath;
			this.name = path.basename(filePath);
			document.title = `Zooniverse: ${ this.name }`;
			this.load();
		});

		this.messageHandlers = {};
	}

	prepareSocket() {
		if (this._url) {
			return this._url;
		}
		this._sock = zmq.socket('pull');
		let success = false;
		let lastEx = null;
		for (let port = PORT_START; port < PORT_START + PORT_SPAN; ++port) {
			try {
				this._url = `tcp://127.0.0.1:${ port }`;
				this._sock.bindSync(this._url);
				success = true;
				break;
			}
			catch (ex) {
				// fine, most likely a result of multiple files open in different windows
				// save a copy in case we can't find any usable port,
				// in which case we're more interested in what the message says
				lastEx = ex;
			}
		}
		if (!success) {
			throw lastEx;
		}
		this._sock.on('message', (msg) => {
			const doc = JSON.parse(msg.toString('utf-8'));
			if (this.messageHandlers[doc.collate]) {
				this.messageHandlers[doc.collate](doc, () => delete this.messageHandlers[doc.collate]);
			}
		})
		return this._url;
	}

	handleMessage(doc: mixed, done) {
		const OPACITY = {
			'MIN': 0.1,
			'MAX': 1
		};
		// interrupted load, don't use it
		if (doc.collate !== this.loadID) {
			return;
		}
		const cont = this.shadowRoot.querySelector('#prnt');
		if (doc.est_lines) {
			this.estLines = doc.est_lines;
		}
		else {
			this.data = doc;
			cont.style.opacity = Math.max(OPACITY.MIN, Math.min(OPACITY.MAX, doc.classifications.count / this.estLines));

			if (doc.dates && doc.dates.start && doc.dates.end) {
				this.dateRange = doc.dates;
			}
			if (doc.final) {
				if (!this.unfilteredDateRange && doc.dates) {
					this.unfilteredDateRange = doc.dates;
				}
				cont.style.opacity = 1;
				this.loadID = null;
				done();
			}
		}
	}

	get
	classDailyLines(): Array<TimeSeriesLine> {
		const obs = this.get('detail.TimeSeriesObserver', {});
		/*
		 * day:
		 * 	gini { daily, cumulative }
		 * 	logged_in { true, false }
		 * 	mean_classification_duration
		 * 	users { a: 42, b: 6 }
		 */
		return [
			{
				'label': 'classification count (daily)',
				'data': Object.keys(obs).map((day) => [
					new Date(day),
					obs[day].logged_in.true + obs[day].logged_in.false
				]),
				'interpolate': 'stepBefore',
				'svg': {
					'stroke': '#339'
				}
			},
			{
				'label': 'classification count (daily, logged in)',
				'data': Object.keys(obs).map((day) => [
					new Date(day),
					obs[day].logged_in.true
				]),
				'interpolate': 'stepBefore',
				'svg': {
					'stroke': '#669',
					'stroke-dasharray': '4 1'
				}
			}
		];
	}

	get
	classDailyDurations(): Array<TimeSeriesLine> {
		const obs = this.get('detail.TimeSeriesObserver', {});
		return [
			{
				'label': 'mean classification duration (daily)',
				'data': Object.keys(obs).map((day) => [
					new Date(day),
					obs[day].mean_classification_duration
				]),
				'interpolate': 'stepBefore',
				'svg': {
					'stroke': '#339'
				}
			}
		];
	}

	get
	classDailyGini(): Array<TimeSeriesLine> {
		const obs = this.get('detail.TimeSeriesObserver', {});
		return [
			{
				'label': 'Gini coefficient (daily)',
				'data': Object.keys(obs).map((day) => [
					new Date(day),
					obs[day].gini.daily
				]),
				'interpolate': 'stepBefore',
				'svg': {
					'stroke': '#339'
				}
			},
			{
				'label': 'Gini coefficient (cumulative)',
				'data': Object.keys(obs).map((day) => [
					new Date(day),
					obs[day].gini.cumulative
				]),
				'svg': {
					'stroke': '#669',
					'stroke-dasharray': '4 1'
				}
			}
		];
	}

	/**
	 * Shell out to the python script that analyzes csv files and record results as they come in over the 0mq socket
	 */
	load(filter: string = '', dates: ?[Date, Date] = null): void {
		// compare string date from python record to js date from picker
		const compDate = (str, dt) => {
			const dt2 = new Date(str);
			return dt.getFullYear() === dt2.getFullYear() && dt.getMonth() === dt2.getMonth() && dt.getDay() === dt2.getDay();
		};
		const useDates = dates && dates[0] && dates[1]
			// avoid providing dates when they match the bounds anyway.
			// the behavior might subtly differ e.g., w/r/t whether un-dated rows pass or fail
			&& (!compDate(this.dateRange.start, dates[0]) || !compDate(this.dateRange.end, dates[1]));

		this.loadID = crypto.randomBytes(MSG_ID_BYTES).toString('base64');
		this.zmqUrl = this.prepareSocket();
		if (this.loadProc) {
			this.loadProc.stdin.pause();
			this.loadProc.kill();
		}
		// link relative to packaged electron bundle is different than during dev
		const base = remote.app.isPackaged ? path.resolve(path.join(__dirname, '..', '..', 'resources')) : '.';
		// map platform to env/{dir}, which, annoyingly, is always somewhat different
		const platform = process.platform === 'darwin' ? 'mac' : process.platform.replace(/\d+$/, '');
		this.binPath = `${ base }/env/${ platform }`;
		const load = this.spawnPython('summarize',
			{
				filter,
				'path': this.path,
				// $FlowFixMe - it doesn't understand the useDates indirect check on `dates` viability
				'min_date': useDates ? dates[0].toISOString() : null,
				// $FlowFixMe
				'max_date': useDates ? dates[1].toISOString() : null
			},
			this.handleMessage.bind(this)
		);
		this.loadID = load.collate;
		this.loadProc = load.proc;
	}

	/**
	 * @param {string} name python script to call, should map to ${name}-api.py in scripts/
	 * @param {mixed} args dict to supply as arguments
	 * @param {Function} handler callback for responses to this request, probably multiple.
	 *                           receives (json, done) as args and should call done() when
	 *                           it is the last response expected from the request.
	 */
	spawnPython(name, args, handler) {
		args.collate = crypto.randomBytes(MSG_ID_BYTES).toString('base64');
		args.url = this.zmqUrl;
		console.log('sp', name, args);
		const proc = spawn(`${ this.binPath }/${ name }-api`, [ JSON.stringify(args) ]);
		proc.stderr.on('data', (data) => {
			const msg = data.toString('utf-8');
			reportError(new Error(msg));
			console.error(msg); // eslint-disable-line no-console
		});
		this.messageHandlers[args.collate] = handler;
		return { proc, 'collate': args.collate };
	}

	applyFilters({ detail }: { detail: { text: string, dates: [ Date, Date ] } }) {
		this.load(detail.text, detail.dates);
	}

	/**
	 * Click handler to switch between tabs
	 */
	changeTab({ path: domPath }: { path: Array<Element> }) {
		const assertEl = (el): HTMLElement => {
			if (!el || !(el instanceof HTMLElement)) {
				throw new Error('expected element not found');
			}
			return el;
		};

		const tab = assertEl(domPath.find((el) => el.tagName === 'LI'));
		if (tab.classList.contains('active')) {
			return;
		}
		const activeTab = assertEl(this.shadowRoot.querySelector('#tabs .active'));
		activeTab.classList.remove('active');
		tab.classList.add('active');
		const currentArea = assertEl(this.shadowRoot.querySelector('#bodies .active'));
		currentArea.classList.remove('active');
		const newArea = assertEl(this.shadowRoot.querySelector(`#${ tab.classList.item(0) }`));
		newArea.classList.add('active');
	}

	/**
	 * Shortcut to get an item from the data structure. Useful so the calling code doesn't have to check first if the data is ready
	 *
	 * @param {string} objPath which data to get, represented by a string with dots indicating depth in the structure
	 * @param {T} def default to return when the data is not available
	 *
	 * @returns {T}
	 */
	get<T>(objPath: string, def: T): T {
		if (!this.data) {
			return def;
		}
		let targ = this.data;
		const parts = objPath.split('.');
		for (let idx = 0; idx < parts.length; ++idx) {
			if (!targ[parts[idx]]) {
				return def;
			}
			targ = targ[parts[idx]];
		}
		return (targ: any);
	}

	/**
	 * Get data that's expected to be numeric
	 *
	 * @param {string} objPath location in data structure
	 * @param {int} maximumFractionDigits number of trailing digits to show for floats
	 *
	 * @returns {string} representation of number in the user's locale
	 */
	getNumber(objPath: string, maximumFractionDigits: number = DEFAULT_FRACTION_DIGITS) {
		return this.get(objPath, 0).toLocaleString([], { maximumFractionDigits });
	}

	/**
	 * Get a string representation of the day of a data that's expected to be a datetime
	 *
	 * @param {string} objPath location in data structure
	 */
	getDay(objPath: string): string {
		const str = this.get(objPath);
		if (!str) {
			return 'unknown';
		}
		const dt = new Date(str);
		return [ dt.getFullYear(), dt.getMonth() + 1, dt.getDate() ].join('-');
	}

	/**
	 * Get a component displaying basic statistical info, where available
	 *
	 * @param {string} objPath location in data structure
	 *
	 * @returns {any} ZnRange elemnt where data is available, otherwise an empty string.
	 */
	getRange(objPath: string): string | ZnRange {
		const val = this.get(objPath, '');
		if (!val || !val.mean) {
			return '';
		}
		const range = new ZnRange;
		Object.entries(val).forEach(([ k, v ]) => range[k] = v);
		return range;
	}

	/**
	 * Count users where available
	 */
	getTotalUsers(): string {
		if (!this.data) {
			return '0';
		}
		return (this.data.users.logged_in.true + this.data.users.logged_in.false).toLocaleString();
	}

	/**
	 * Template helper for building a condensed table of workflow versions, grouping
	 * those that make up a small portion into one entry.
	 *
	 * @param {object} versions map of version: count
	 * @param {number} pct percentage of the total count of classification (so far)
	 *                 "non-notable" versions are allowed to take up, combined
	 * @param {function} notableCb called for each version above the threshold, in
	 *                             descending order by count, expected to return
	 *                             html describing it
	 * @param {function} notCb called once only if there are any versions grouped
	 *                         as "non-notable", with a list of ids sorted
	 *                         descending by their contribution. expected to
	 *                         return html describing it
	 *
	 * @return {array} html
	 */
	partitionByNotability(
		versions: { [string]: number },
		pct: number,
		notableCb: ({ version: string, count: number }) => string,
		notCb: (Array<string>) => string
	): Array<string> {
		if (!this.data) {
			return [];
		}
		const thresh = this.data.classifications.count * pct / 100;
		let sum = 0;
		const notable: {
			'true': Array<{ version: string, count: number }>,
			'false': Array<string>
		} = {
			'true': [],
			'false': []
		};
		Object.entries(versions).reverse().forEach(([ version, count ]) => {
			const intCount = parseInt(count, 10); // already definitely a number, but for flow's peace of mind
			sum += intCount;
			if (sum > thresh) {
				notable.true.unshift({ version, 'count': intCount });
			}
			else {
				notable.false.unshift(version);
			}
		});
		const rv = notable.true.map(notableCb);
		if (notable.false.length > 0) {
			rv.push(notCb(notable.false));
		}
		return rv;
	}

	updateExportParams({ target }) {
		this.shadowRoot.querySelector('zn-export')[target.name] = {
			'all': target.allSelected,
			'selected': target.selected
		};
	}

	render() {
		return html`<style>${ css }</style> ${ tpl(this) }`;
	}
}
window.customElements.define('zn-file-view', ZnFileView);
