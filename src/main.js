// not all require() can be global since some dev-dependencies are not bundled
/* eslint-disable global-require */

// spec for main window
const MAIN = {
	'width': 600,
	'height': 300
};

const { app, BrowserWindow } = require('electron');


if (process.env.NODE_ENV && (/^dev/i).test(process.env.NODE_ENV)) {
	require('electron-context-menu')();
	require('electron-debug')();
	// hot-reloading. run `yarn watch` in another terminal to re-build bundle
	require('electron-reload')(
		`${ __dirname }/dist`,
		{ 'electron': require(`${ __dirname }/../node_modules/electron`) }
	);
}


let mainWindow = null;

const createWindow = () => {
	mainWindow = new BrowserWindow(MAIN);
	mainWindow.setMenu(null);
	mainWindow.loadFile('index.html');

	mainWindow.on('closed', () => mainWindow = null);
};

app.on('ready', createWindow);

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	if (mainWindow === null) {
		createWindow();
	}
});
