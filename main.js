// not all require() can be global since some dev-dependencies are not bundled
/* eslint-disable global-require */

// spec for main window
const MAIN = {
	'width': 600,
	'height': 300,
	'resizable': false
};

const { app, BrowserWindow } = require('electron');

let mainWindow = null;

const createWindow = () => {
	mainWindow = new BrowserWindow(MAIN);
	mainWindow.setMenu(null);
	mainWindow.loadFile('index.html');

	mainWindow.on('closed', () => mainWindow = null);
};

app.on('ready', createWindow);

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	if (mainWindow === null) {
		createWindow();
	}
});
