const easyImport = require('postcss-easy-import');

module.exports = {
	'plugins': [
		easyImport({
			'extensions': [
				'.pcss',
				'.css',
				'.postcss',
				'.sss'
			]
		})
	]
};
