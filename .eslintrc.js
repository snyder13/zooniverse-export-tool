// adapted from https://gist.github.com/adrianhall/70d63d225e536b4563b2
module.exports = exports = {
	'env': {
		'es6': true,
		'amd': true,
		'browser': true,
		'node': true
	},
	'parser': 'babel-eslint',
	'parserOptions': {
		'sourceType': 'module'
	},

	'root': true,

	'overrides': {
		'files': 'src/**/*.js'
	},
	'plugins': [ 'flowtype' ],
	'settings': {
		'html/html-extensions': ['.html', '.lit'],
	},

	'extends': [
		'eslint:recommended',
		'plugin:flowtype/recommended'
	],

	'rules': {
		//		'flowtype-errors/show-errors': 2,
		// Possible Errors (overrides from recommended set)
		'no-extra-parens': 'off',
		'no-unexpected-multiline': 'error',

		// All JSDoc comments must be valid
		'valid-jsdoc': [ 'off', {
			'requireReturn': false,
			'requireReturnDescription': false,
			'requireParamDescription': true,
			'prefer': {
				'return': 'returns'
			}
		} ],

		// Best Practices

		// Allowed a getter without setter, but all setters require getters
		'accessor-pairs': [ 'error', {
			'getWithoutSet': false,
			'setWithoutGet': true
		} ],
		'block-scoped-var': 'error',
		'consistent-return': 'error',
		'curly': 'error',
		'default-case': 'error',
		// the dot goes with the property when doing multiline
		'dot-location': [ 'error', 'property' ],
		'dot-notation': 'error',
		'eqeqeq': [ 'error', 'smart' ],
		'guard-for-in': 'error',
		'no-alert': 'error',
		'no-caller': 'error',
		'no-case-declarations': 'error',
		'no-div-regex': 'error',
		'no-else-return': 'error',
		'no-labels': 'error',
		'no-empty-pattern': 'error',
		'no-eq-null': 'error',
		'no-eval': 'error',
		'no-extend-native': 'error',
		'no-extra-bind': 'error',
		'no-floating-decimal': 'error',
		'no-implicit-coercion': [ 'error', {
			'boolean': true,
			'number': true,
			'string': true
		} ],
		'no-implied-eval': 'error',
		'no-invalid-this': 'error',
		'no-iterator': 'error',
		'no-labels': 'error',
		'no-lone-blocks': 'error',
		'no-loop-func': 'error',
		'no-magic-numbers': ['error', { 'ignore': [-1, 0, 1, 2, 100] } ],
		'no-multi-spaces': 'error',
		'no-multi-str': 'error',
		'no-native-reassign': 'error',
		'no-new-func': 'error',
		'no-new-wrappers': 'error',
		'no-new': 'error',
		'no-octal-escape': 'error',
		'no-param-reassign': 'error',
		'no-process-env': 'off',
		'no-proto': 'error',
		'no-redeclare': 'error',
		'no-return-assign': 'off',
		'no-script-url': 'error',
		'no-self-compare': 'error',
		'no-throw-literal': 'error',
		'no-unused-expressions': 'error',
		'no-useless-call': 'error',
		'no-useless-concat': 'error',
		'no-void': 'error',
		'no-with': 'error',
		'radix': 'error',
		'vars-on-top': 'error',
		// Enforces the style of wrapped functions
		'wrap-iife': [ 'error', 'outside' ],
		'yoda': 'error',

		// Strict Mode - for ES6, never use strict.
		'strict': [ 'error', 'never' ],

		// Variables
		'init-declarations': [ 'error', 'always' ],
		'no-catch-shadow': 'error',
		'no-delete-var': 'error',
		'no-label-var': 'error',
		'no-shadow-restricted-names': 'error',
		'no-shadow': 'error',
		// We require all vars to be initialized (see init-declarations)
		// If we NEED a var to be initialized to undefined, it needs to be explicit
		'no-undef-init': 'off',
		'no-undef': 'error',
		'no-undefined': 'off',
		'no-unused-vars': 'error',
		// Disallow hoisting - let & const don't allow hoisting anyhow
		'no-use-before-define': 'error',

		// Node.js and CommonJS
		'callback-return': [ 'error', [ 'callback', 'next' ]],
		'global-require': 'error',
		'handle-callback-err': 'error',
		'no-mixed-requires': 'error',
		'no-new-require': 'error',
		// Use path.concat instead
		'no-path-concat': 'error',
		'no-process-exit': 'error',
		'no-restricted-modules': 'off',
		'no-sync': 'off',
		'no-console': 'warn',

		// ECMAScript 6 support
		'arrow-body-style': [ 'error', 'as-needed' ],
		'arrow-parens': [ 'error', 'always' ],
		'arrow-spacing': [ 'error', { 'before': true, 'after': true }],
		'constructor-super': 'off', // doesn't do the right thing due to weird lit-loader rewriting of class (extends not listed in source)
		'generator-star-spacing': [ 'error', 'before' ],
		'no-confusing-arrow': 'off',
		'no-constant-condition': 'error',
		'no-class-assign': 'error',
		'no-const-assign': 'error',
		'no-dupe-class-members': 'error',
		'no-this-before-super': 'error',
		'no-var': 'error',
		'object-shorthand': [ 'error', 'always' ],
		'prefer-arrow-callback': 'error',
		'prefer-spread': 'error',
		'prefer-template': 'error',
		'require-yield': 'error',

		// Stylistic - everything here is a erroring because of style.
		'array-bracket-spacing': [ 'error', 'always' ],
		'block-spacing': [ 'error', 'always' ],
		'brace-style': [ 'error', 'stroustrup', { 'allowSingleLine': false } ],
		'camelcase': 'error',
		'comma-spacing': [ 'error', { 'before': false, 'after': true } ],
		'comma-style': [ 'error', 'last' ],
		'computed-property-spacing': [ 'error', 'never' ],
		'consistent-this': [ 'error', 'self' ],
		'eol-last': 'error',
		'func-names': 'error',
		'func-style': [ 'error', 'declaration', { 'allowArrowFunctions': true } ],
		'id-length': [ 'error', { 'min': 2, 'max': 32 } ],
		'indent': [ 'error', 'tab' ],
		'jsx-quotes': [ 'error', 'prefer-double' ],
		'linebreak-style': [ 'error', 'unix' ],
		'lines-around-comment': 'off',
		'max-depth': [ 'error', 8 ],
		'max-len': [ 'error', 132 ],
		'max-nested-callbacks': [ 'error', 8 ],
		'max-params': [ 'error', 8 ],
		'new-cap': 'error',
		'new-parens': 'off',
		'no-array-constructor': 'error',
		'no-bitwise': 'off',
		'no-continue': 'off',
		'no-inline-comments': 'off',
		'no-lonely-if': 'error',
		'no-mixed-spaces-and-tabs': 'error',
		'no-multiple-empty-lines': 'error',
		'no-negated-condition': 'off',
		'no-nested-ternary': 'off',
		'no-new-object': 'error',
		'no-plusplus': 'off',
		'no-spaced-func': 'error',
		'no-ternary': 'off',
		'no-trailing-spaces': 'error',
		'no-underscore-dangle': 'off',
		'no-unneeded-ternary': 'error',
		'no-warning-comments': 'warn',
		'object-curly-spacing': [ 'error', 'always' ],
		'one-var': 'off',
		'operator-assignment': [ 'error', 'always' ],
		'operator-linebreak': [ 'error', 'before' ],
		'padded-blocks': 'off',
		'quote-props': [ 'off' ],
		'quotes': [ 'error', 'single' ],
		'require-jsdoc': [ 'off', {
			'require': {
				'FunctionDeclaration': true,
				'MethodDefinition': true,
				'ClassDeclaration': false
			}
		} ],
		'semi-spacing': [ 'error', { 'before': false, 'after': true }],
		'semi': [ 'error', 'always' ],
		'sort-vars': 'off',
		'keyword-spacing': [ 'error', { 'before': true, 'after': true } ],
		'space-before-blocks': [ 'error', 'always' ],
		'space-before-function-paren': [ 'error', {
			'anonymous': 'never',
			'named': 'never',
			'asyncArrow': 'always'
		} ],
		'space-in-parens': [ 'error', 'never' ],
		'space-infix-ops': [ 'error', { 'int32Hint': true } ],
		'space-unary-ops': 'error',
		'spaced-comment': [ 'error', 'always' ],
		'wrap-regex': 'error'
	}
};
