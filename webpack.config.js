const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
	'target': 'electron-renderer',
	'mode': 'none',
	'entry': './src',
	'devtool': 'source-map',
	'output': {
		'path': path.resolve('./static'),
		'filename': 'bundle.js'
	},
	'node': {
		'__dirname': false,
		'__filename': false
	},
	'plugins': [
		new CopyWebpackPlugin([ {
			'from': 'assets/**',
			'context': path.resolve('./src'),
			'to': path.resolve('./static')
		} ])
	],
	'module': {
		'rules': [
			{
				'test': /\.js$/,
				'use': [ 'babel-loader' ]
			},
			{
				'test': /\.pcss$/,
				'use': [ 'text-loader', 'postcss-loader' ]
			},
			{
				'test': /\.node$/,
				'loader': 'native-ext-loader',
				'options': {
					'basePath': process.env.NODE_ENV && (/dev/i).test(process.env.NODE_ENV)
						? [ 'static' ]
						: [ '..', '..', 'static' ]
				}
			}
		]
	}
};
