import sys
import itertools
import json
import collections
from csv_rows import csv_rows
from observers import *

def num_fmt(num):
    return '%.2f' % num if num is not None else 'None'

# we have values stored in a descending list for other reasons, saves a infinitesimal bit of time to specify the calculation for that case
def gini_presorted_desc(values):
    if not values:
        return None

    height = 0
    area = 0
    for val in reversed(values):
        height += val
        area += height - val / 2.
    fair_area = height * len(values) / 2
    return (fair_area - area) / fair_area

# generic statistics about how often each value occurred in each column
class UniqueCounter:
    row_count = 0
    counts = {}
    observers = []
    header = None
    called = 0

    # add a callback that is able to reject entries by returning False
    def add_observer(self, obs):
        self.observers.append(obs)

    def add(self, row):
        self.called += 1
        if self.header is None:
            self.header = row.keys()

        for obs in self.observers:
            if not obs.accept(row):
                return

        for obs in self.observers:
            obs.record(row)

        self.row_count += 1
        for key, val in row.items():
            if key not in self.counts:
                self.counts[key] = {}

            # more detailed stats from JSON can happen elsewhere, we'll just mark it as "seen something"
            if not isinstance(val, str):
                if '_any' not in self.counts[key]:
                    self.counts[key]['_any'] = 0
                self.counts[key]['_any'] += 1
                continue

            # times we've seen this value in this column
            if val not in self.counts[key]:
                self.counts[key][val] = 0
            self.counts[key][val] += 1

    # iterate the most common values for a column
    def ranked(self, key):
        if not key in self.counts:
            return
        for val in sorted(self.counts[key], key = self.counts[key].get, reverse = True):
            yield val, self.counts[key][val]

    # grab the N most-common values for a column
    def top(self, key, n):
        if n is None:
            return self.ranked(key)
        return itertools.islice(self.ranked(key), n)


    # min and max of the values in a column (not the counts)
    def bounds(self, key):
        if not key in self.counts:
            return [ None, None ]
        return min(self.counts[key].keys()), max(self.counts[key].keys())

    # number of distinct values for a column
    def unique_count(self, key):
        if not key in self.counts:
            return 0
        return len(self.counts[key].keys())

    def all_unique_counts(self):
        return { key: self.unique_count(key) for key in self.counts }

    # count the return values of the supplied callback when applied to the values in a list
    # useful to tally some property, like whether a user is logged in
    def partition_count(self, key, cb):
        if not key in self.counts:
            return { True: 0, False: 0 }
        rv = collections.Counter([ cb(val) for val in self.counts[key].keys() ])
        if False not in rv:
            rv[False] = 0
        if True not in rv:
            rv[True] = 0
        return rv

    def data(self, user_rank_limit = None):
        logged_in = self.partition_count('user_name', lambda name: not name.startswith('not-logged-in'))
        dates = self.bounds('created_at')
        classification_bounds = self.bounds('classification_id')
        return {
            'unique_counts': self.all_unique_counts(),
            'classifications': {
                'count': self.row_count,
                'min': classification_bounds[0],
                'max': classification_bounds[1]
            },
            'subjects': {
                'count': self.unique_count('subject_ids')
            },
            'users': {
                'logged_in': logged_in,
                'ips': self.unique_count('user_ip'),
                'ranked': list(self.top('user_name', user_rank_limit)),
                'gini_coefficient_classifications': gini_presorted_desc([ count for _, count in self.ranked('user_name') ])
            },
            'dates': {
                'start': dates[0],
                'end': dates[1]
            },
            'detail': { obs.__class__.__name__: obs.data() for obs in self.observers }
        }

def table(it, indent = '\t'):
    print(indent + ('\n' + indent).join([ 'id: %s, count: %d' % (id, count) for id, count in it ]))

def print_summary(counter, _final, user_rank_limit = 10):
    data = counter.data(user_rank_limit)
    print('\n-------------')
    print('\n%d classifications' % data['classifications']['count'])

    for obs in counter.observers:
        obs.summary()

    print('\n%d subjects by %d classifiers' % (data['subjects']['count'], data['users']['logged_in'][True] + data['users']['logged_in'][False]))

    print('%d logged in and %d not logged in, from %d unique IP addresses' % (data['users']['logged_in'][True], data['users']['logged_in'][False], data['users']['ips']))

    print('\nmost prolific classifiers:')
    table(data['users']['ranked'])

    print('\nGini coefficient for classifications by user: %s' % num_fmt(data['users']['gini_coefficient_classifications']))

    print('\nClassifications were collected between %s and %s' % (data['dates']['start'], data['dates']['end']))
    print('Highest classification id: %s' % data['classifications']['max'])

    print('\nunique column values:')
    table(data['unique_counts'].items())

def run(args, reporter):
    if not 'path' in args:
        raise Exception('you must provide a filename')

    counter = UniqueCounter()
    if 'omit_non_live' in args and args['omit_non_live']:
        counter.add_observer(NonLiveObserver())
    if 'min_date' in args or 'max_date' in args:
        counter.add_observer(DateRangeFilterObserver(args.get('min_date'), args.get('max_date')))
    if 'filter' in args and args['filter'] != '':
        counter.add_observer(TextFilterObserver(args['filter']))
    counter.add_observer(TypesObserver())
    counter.add_observer(ClassificationObserver())
    counter.add_observer(WorkflowObserver())
    counter.add_observer(TimeSeriesObserver())

    for idx, row in enumerate(csv_rows(args['path'])):
        if idx > 0 and 'report_every' in args and args['report_every'] is not None and idx % args['report_every'] == 0:
            rv = reporter(counter, False)
            if rv is not None:
                args['report_every'] = rv
        counter.add(row)

    reporter(counter, True)

def main():
    run({
        'path': sys.argv[1],
        'report_every': None,
        'omit_non_live': False,
#        'max_date': '2010-01-23T19:48:06.749Z'
    }, print_summary)

if __name__ == '__main__':
    main()
