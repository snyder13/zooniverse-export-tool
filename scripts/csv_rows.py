import csv
import json

# needed to fit in some JSON columns
csv.field_size_limit(2**20)

def csv_rows(filename, cols = None):
    with open(filename, 'r') as fh:
        reader = csv.reader(fh)
        header = next(reader, None)
        if header is None:
            return

        for row in reader:
            parsed = {}
            # make a dict combining values with header
            for idx, key in enumerate(header):
                if cols is not None and key not in cols:
                    continue
                # parse meta for further stats
                if key in ('metadata', 'meta_json'):
                    # normalize column name
                    parsed['metadata'] = json.loads(row[idx])
                    # easier later to determine stats w/ the user name stored directly with this document
                    parsed['metadata']['user_name'] = row[header.index('user_name')]
                else:
                    parsed[key] = row[idx]
            yield parsed
