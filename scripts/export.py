import sys
import json
from csv_rows import csv_rows

class JsonExporter:
    props = None
    output = None
    lines = False
    last = None

    def __init__(self, cols, output, props = {}):
        self.props = props
        self.output = output
        self.lines = props.get('lines', False)
        self.header = props.get('header', True)
        if (self.header):
            if self.lines:
                output(json.dumps(cols))
            else:
                output('{"columns":%s,"rows":[' % ( json.dumps(cols) ))
        elif (not self.lines):
            output('[')

    def line(self, row):
        if (self.lines):
            self.output(json.dumps(list(row.values())))
        else:
            if (self.last):
                self.output(self.last + ',')
            self.last = json.dumps(list(row.values()) if self.header else row)

    def finish(self):
        if (not self.lines):
            self.output('%s]%s' % (
                self.last if self.last is not None else '',
                '}' if self.header and not self.lines else ''
            ))


def run(args, output):
    exporter = getattr(sys.modules[__name__], args['exporter'][0])(args['columns'], output, args['exporter'][1])
    for idx, row in enumerate(csv_rows(args['path'], args['columns'])):
        exporter.line(row)
    exporter.finish()

def main():
    for [ header, lines ] in [[ True, True ], [ True, False ], [ False, True ], [ False, False ]]:
        print('header: %s, lines: %s' % ( header, lines ))
        run({
            'path': sys.argv[1],
            'columns': [ 'a', 'c' ],
            'exporter': [ 'JsonExporter', { 'header': header, 'lines': lines } ]
        }, lambda str: print(str, end=''))
        print()

if __name__ == '__main__':
    main()
