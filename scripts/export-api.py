import sys
import json

import zmq
import export

def main():
    args = json.loads(sys.argv[1], strict = False)

    zctx = zmq.Context()

    sock = zctx.socket(zmq.PUSH)
    sock.setsockopt(zmq.SNDTIMEO, 5000)
    sock.connect(args['url'])

    sock.send_json({ 'collate': args['collate'], 'args': args })

if __name__ == '__main__':
    main()
