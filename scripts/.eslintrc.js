module.exports = exports = {
	'parserOptions': {
		'sourceType': 'script'
	},
	'rules': {
		'strict': [ 'warn', 'global' ],
		'no-console': 'off',
		'no-process-exit': 'off'
	}
};
