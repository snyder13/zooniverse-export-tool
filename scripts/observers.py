import re
import statistics
from datetime import datetime

iso_date_fmt = r'%Y-%m-%dT%H:%M:%S.%fZ'

# remove rows for which the live_project field of the metadata is false
# assumes true when the key is missing entirely
class NonLiveObserver():
    filtered = 0

    def summary(self):
        print('removed %d non-live-classifications' % self.filtered)

    def data(self):
        return { 'filtered': self.filtered }

    def accept(self, row):
        if not 'metadata' in row or not 'live_project' in row['metadata'] or row['metadata']['live_project']:
            return True
        self.filtered += 1
        return False

    def record(self, row):
        pass

class ClassificationObserver:
    logged_in = { False: 0, True: 0 }
    per_subject = {}
    per_user = {}

    def summary(self):
        data = self.data()
        print('%d classificiations were from logged-in users, %d from not-logged-in users' % (self.logged_in[True], self.logged_in[False]))
        print('%s mean classifications per subject (%s median)' % (num_fmt(data['per_subject']['mean']), num_fmt(data['per_subject']['median'])))
        print('most-classified subject: %s, least: %s' % (num_fmt(data['per_subject']['max']), num_fmt(data['per_subject']['min'])))
        print('mean classifications per user: %s, median: %s' % (num_fmt(data['per_user']['mean']), num_fmt(data['per_user']['median'])))

    def data(self):
        psv = self.per_subject.values()
        per_subject = {
            'mean': None,
            'median': None,
            'min': None,
            'max': None
        }
        if psv:
            per_subject = {
                'mean': statistics.mean(psv),
                'median': statistics.median(psv),
                'min': min(psv),
                'max': max(psv)
            }

        puv = self.per_user.values()
        per_user = {
            'mean': None,
            'median': None,
            'min': None,
            'max': None
        }
        if puv:
            per_user = {
                'mean': statistics.mean(puv),
                'median': statistics.median(puv),
                'min': min(puv),
                'max': max(puv)
            }

        return {
            'per_subject': per_subject,
            'per_user': per_user,
            'logged_in': self.logged_in
        }

    # @TODO optional duplicate filter on (subject_id, user_id, workflow_id)
    @staticmethod
    def accept(_row):
        return True

    def record(self, row):
        if row['metadata']['user_name'].startswith('not-logged-in'):
            self.logged_in[False] += 1
        else:
            self.logged_in[True] += 1

        if not row['subject_ids'] in self.per_subject:
            self.per_subject[row['subject_ids']] = 0
        self.per_subject[row['subject_ids']] += 1

        if not row['metadata']['user_name'] in self.per_user:
            self.per_user[row['metadata']['user_name']] = 0
        self.per_user[row['metadata']['user_name']] += 1

class WorkflowObserver:
    by_id = {}

    def summary(self):
        print('workflows:')
        for workflow, versions in self.data().items():
            print('\tid: %s, count: %d, versions:' % (workflow, sum([ count for _, count in versions.items() ])))
            table(versions.items(), '\t\t')

    def data(self):
        for workflow, versions in self.by_id.items():
            self.by_id[workflow] = { key: versions[key] for key in sorted(versions, key = versions.get, reverse = True) }
        return self.by_id

    @staticmethod
    def accept(_row):
        return True

    def record(self, row):
        if not row['workflow_id'] in self.by_id:
            self.by_id[row['workflow_id']] = {};
        if not row['workflow_version'] in self.by_id[row['workflow_id']]:
            self.by_id[row['workflow_id']][row['workflow_version']] = 0;
        self.by_id[row['workflow_id']][row['workflow_version']] += 1;

        return True

class TypesObserver:
    columns = {}
    init = False

    def summary(self):
        pass

    def data(self):
        return { key: val.__name__ for key, val in self.columns.items() }

    @staticmethod
    def accept(_row):
        return True

    def record(self, row):
        if not self.init:
            self.init = True
            for col in row.keys():
                self.columns[col] = None

        for col, colType in self.columns.items():
            if colType in (str, dict):
                continue;

            val_type = row[col].__class__
            if val_type == str:
                try:
                    float(row[col])
                    val_type = float
                except ValueError:
                    pass

            self.columns[col] = val_type

class TextFilterObserver:
    text = ''
    counts = {}

    def __init__(self, text):
        self.text = text.lower()

    def accept(self, row):
        matched = False
        for col, val in row.items():
            str_val = json.dumps(val) if isinstance(val, dict) else str(val)
            if self.text in str_val.lower():
                if not col in self.counts:
                    self.counts[col] = 0
                self.counts[col] += 1
                matched = True
        return matched

    def summary(self):
        print('filter matches:')
        table(self.counts.items())

    def data(self):
        return self.counts

    def record(self, row):
        pass

class DateRangeFilterObserver:
    min_date = None
    max_date = None
    counts = {
        'passed': 0,
        'failed': 0
    }

    def __init__(self, min_date, max_date):
        if min_date is not None:
            self.min_date = datetime.strptime(min_date, iso_date_fmt)
        if max_date is not None:
            self.max_date = datetime.strptime(max_date, iso_date_fmt)

    def accept(self, row):
        if not 'metadata' in row or not 'started_at' in row['metadata']:
            self.counts['passed'] += 1
            return True
        dt = datetime.strptime(row['metadata']['started_at'], iso_date_fmt)
        if (self.min_date is not None and dt < self.min_date) or (self.max_date is not None and dt > self.max_date):
            self.counts['failed'] += 1
            return False
        self.counts['passed'] += 1
        return True

    def summary(self):
        print('date filter:')
        print(self.counts);

    def data(self):
        return self.counts

    def record(self, row):
        pass

class TimeSeriesObserver:
    day_only = r'\s.+$'
    buckets = {}

    @staticmethod
    def accept(_row):
        return True

    @staticmethod
    def gini(vals):
        height, area = 0, 0
        for value in sorted(vals):
            height += value
            area += height - value / 2.
        fair_area = height * len(vals) / 2
        return (fair_area - area) / fair_area

    @staticmethod
    def summary():
        pass

    def data(self):
        gini_running = {}
        for day in self.buckets:
            self.buckets[day]['mean_classification_duration'] = statistics.mean(self.buckets[day]['classifications'])
            self.buckets[day]['gini'] = {
                'daily': self.gini(self.buckets[day]['users'].values())
            }
            for user in self.buckets[day]['users']:
                if not user in gini_running:
                    gini_running[user] = 0
                gini_running[user] += self.buckets[day]['users'][user]
            self.buckets[day]['gini']['cumulative'] = self.gini(gini_running.values())

        # remove list of classifications for consistent minimal export format. we might still need it later when doing multiple reports of the same data, so it's not deleted
        return {
            day: {
                k: self.buckets[day][k] for k in self.buckets[day] if k != 'classifications'
            } for day in self.buckets
        }

    def record(self, row):
        day = re.sub(self.day_only, '', row['created_at'])
        if not day in self.buckets:
            self.buckets[day] = {
                'logged_in': {
                    True: 0,
                    False: 0
                },
                'users': {},
                'classifications': []
            }
        self.buckets[day]['logged_in'][not row['user_name'].startswith('not-logged-in')] += 1

        if not row['user_name'] in self.buckets[day]['users']:
            self.buckets[day]['users'][row['user_name']] = 0
        self.buckets[day]['users'][row['user_name']] += 1

        if 'metadata' in row and 'started_at' in row['metadata'] and 'finished_at' in row['metadata'] and None not in [ row['metadata']['started_at'], row['metadata']['finished_at'] ]:
            self.buckets[day]['classifications'].append(
                (datetime.strptime(row['metadata']['finished_at'], iso_date_fmt) - datetime.strptime(row['metadata']['started_at'], iso_date_fmt))
                .total_seconds()
            )
