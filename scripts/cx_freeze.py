from os import environ
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = [], excludes = [])

base = 'Console'

executables = [
    Executable('summarize-api.py', base=base),
    Executable('export-api.py', base=base)
]

setup(name='zooniverse-export-tool',
    version = environ['VERSION'],
    description = '',
    options = dict(build_exe = buildOptions),
    executables = executables)
