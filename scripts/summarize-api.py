import json
import sys
import os

import zmq
import summarize

# guesstimate mean line size so the UI has a notion of how far into the
# operation we are as lines arrive
#
# not exact, maybe not even close, but the consequences of failure are low
#
# this is fairly quick up to large line counts but unfortunately with my test
# data it doesn't guess that much better when pushed
#
# (python's csv reader only works on text streams, and text streams had .tell()
# optimized out, so we don't have a good way to see how many bytes we've read.
# we do know how many lines)
def est_line_size(path, lines):
    samples = []
    count = 0
    last_nl = False
    seen_header = False
    with open(path, 'rb') as fh:
        while lines > 0:
            ch = fh.read(1)
            if not ch:
                break
            count += 1
            if ch == b'\r' or ch == b'\n' and not last_nl:
                last_nl = True
                if seen_header:
                    lines -= 1
                    samples.append(count)
                else:
                    seen_header = True
                count = 0
            else:
                last_nl = False
    return sum(samples)/len(samples)

def main():
    # (can't find zmq's runtime-defined constants in static analyis)
    # pylint: disable=E1101
    args = json.loads(sys.argv[1], strict = False)

    zctx = zmq.Context()

    sock = zctx.socket(zmq.PUSH)
    sock.setsockopt(zmq.SNDTIMEO, 5000)
    sock.connect(args['url'])

    size = os.path.getsize(args['path'])
    sock.send_json({
        'est_lines': round(size / est_line_size(args['path'], 100)),
        'collate': args.get('collate')
    })

    # lines between summary data collection
    batch = 5000
    # except the first time, do it more quickly to get *something* to the interface for placeholders
    first_batch = 50
    specified_report_every = 'report_every' in args

    def send(counter, final):
        data = counter.data(100)
        data['final'] = final
        data['collate'] = args.get('collate')
        sock.send_json(data)
        if final:
            sock.close(5000) # linger allowance. probably not needed but shouldn't hurt either
        return args['report_every'] if specified_report_every else batch

    # get the first batch of results very soon, just so we can show *something*
    if not specified_report_every:
        args['report_every'] = first_batch
    summarize.run(args, send)

if __name__ == '__main__':
    main()
