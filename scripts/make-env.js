'use strict';
const { exec } = require('child_process');
const { resolve } = require('path');
const fs = require('fs');
const pkg = require('../package');

const run = (cmd) => new Promise((res, rej) => {
	console.log({ cmd });
	exec(cmd, { 'env': process.env }, (err, stdout, stderr) => {
		if (stderr) {
			console.error(stderr);
		}
		if (err) {
			return rej(err);
		}
		console.log(stdout);
		return res(stdout);
	});
});

const isVirtual = process.argv[2] && (/^-v|--virtual$/).test(process.argv[2]);

const getBinName = (bin) => process.env[bin.toUpperCase()] ? process.env[bin.toUpperCase()] : bin;

const dest = resolve(`${ __dirname }/../env/${ process.platform === 'darwin' ? 'mac' : process.platform.replace(/\d+$/, '') }`);

if (!process.env.VERSION) {
	process.env.VERSION = pkg.version;
}

const IMPL_PLATFORMS = {
	'darwin': async () => {
		// there are other bdist options for OS X, bdist_mac seems to be the only one that results in a binary
		// that has correct relative links to the shipped python library
		await run(`${ getBinName('python3') } cx_freeze.py bdist_mac`);
		return run(`cp -r build/${ pkg.name }-${ process.env.VERSION }.app/Contents/MacOS ${ dest }`);
	},
	'linux': async () => {
		const [ pyver ] = (await run(`${ getBinName('python3') } --version`)).match(/\d+[.]\d+/);
		// (only x86_64 is supported anyway, could be hard-coded. meh)
		const machine = (await run('uname -m')).replace(/[\s\r\n]/g, '');
		const src = `build/exe.linux-${ machine }-${ pyver }`;

		await run(`${ getBinName('python3') } cx_freeze.py bdist`);
		return run(`cp -r ${ src } ${ dest }`);
	},
	'virtual': async () => {
		await run(`${ getBinName('virtualenv') } -p ${ getBinName('python3') } ${ dest }`);
		await run(`${ dest }/bin/python3 -m pip install -r ${ __dirname }/../requirements.txt`);
		const chmod = [];
		fs.readdirSync(__dirname)
			.filter((file) => (/-api[.]py$/).test(file))
			.forEach((api) => {
				const runner = `${ dest }/${ api.replace(/[.]py$/, '') }`;
				fs.writeFileSync(runner, `#!/bin/sh
${ dest }/bin/python3 ${ __dirname }/${ api } "$@"
`);
				chmod.push(run(`chmod a+x ${ runner }`));
			});
		return Promise.all(chmod);
	}
//	'win32': true
};

if (!IMPL_PLATFORMS[process.platform]) {
	throw new Error(`unsupported platform ${ process.platform }`);
}

(async () => {
	const restoreCwd = process.cwd();
	process.chdir(__dirname);
	try {
		if (fs.existsSync(dest)) {
			fs.renameSync(dest, `${ dest }.bak`);
		}
		await IMPL_PLATFORMS[isVirtual ? 'virtual' : process.platform]();
		console.log('ok');
	}
	finally {
		process.chdir(restoreCwd);
	}
})();
