/**
 * Copy files in dist/ to a server
 *
 * Will prompt for a destination unless you include it as an argument to the script
 *
 * If you need a non-standard SCP port to copy the files, define SCP_PORT in the environment
 *
 * Will attempt to update meta.json in the destination to include sha1 sums for the uploaded
 * files. This means you can run this script from multiple build locations and they won't step
 * on each other given they don't build the same targets.
 *
 * `server/middleware/download` is designed to read this meta file and offer downloads to
 * users.
 */
'use strict';

const prompt = require('prompt');
const path = require('path');
const fs = require('fs');
const hash = require('sha1-file');
const { execFileSync, spawn } = require('child_process');

const DEFAULT_SCP_PORT = 22;

let packages = {
	'linux': {
		'type': 'AppImage'
	},
	'win': {
		'type': 'exe'
	},
	'darwin': {
		'type': 'dmg'
	}
};


const main = (err, { dest }) => {
	if (err) {
		throw err;
	}

	const dist = path.resolve(`${ __dirname }/../dist`);

	// make an { extname: platform } map for convenience
	const types = Object.entries(packages).reduce((out, [ platform, { type } ]) => {
		out[`.${ type }`] = platform;
		return out;
	}, {});

	// find qualifying files
	const files = [ 'meta.json' ];
	fs.readdirSync(dist).forEach((file) => {
		const platform = types[path.extname(file)];
		if (!platform) {
			return;
		}
		files.push(file);
		packages[platform] = {
			file,
			'sha1sum': hash(`${ dist }/${ file }`),
			'version': file.replace(/^.*?(\d+[.]\d+[.]\d+).*?$/, '$1')
		};
	});

	// remove unfound packages, note files to copy
	packages = Object.entries(packages).reduce((out, [ platform, meta ]) => {
		if (meta.file) {
			out[platform] = meta;
			console.log(platform, meta);
		}
		else {
			console.log(`skipping ${ platform }, no ${ meta.type } found in dist/`);
		}
		return out;
	}, {});

	const port = process.env.SCP_PORT ? process.env.SCP_PORT : DEFAULT_SCP_PORT;
	let metaSrc = null;
	try {
		execFileSync('scp', [ '-P', port, `${ dest }/meta.json`, dist ]);
		console.log('updating existing meta.json');
		metaSrc = `${ dist }/meta.json`;
	}
	catch (ex) {
		console.log('no existing meta.json found, creating a fresh one');
	}

	const meta = { ...(metaSrc ? JSON.parse(fs.readFileSync(metaSrc, { 'encoding': 'utf8' })) : {}), ...packages };
	fs.writeFileSync(`${ dist }/meta.json`, JSON.stringify(meta, null, '\t'));

	const scp = spawn('scp', [ '-v', '-P', port, ...files.map((file) => `${ dist }/${ file }`), dest ]);
	scp.stdin.end();
	scp.stderr.pipe(process.stderr);
	scp.stdout.pipe(process.stdout);
};

if (process.argv[2]) {
	main(null, { 'dest': process.argv[2] });
}
else {
	prompt.start();
	prompt.get({
		'name': 'dest',
		'description': 'Output location? (/local/path or server.org:/remote/path)',
		'default': path.resolve(`${ __dirname }/../server/public/packages`)
	}, main);
}

